package com.MpesaApi.common;

import java.io.File;
import java.io.IOException;

import org.ini4j.Ini;


public class ExternalFile {
	public static String getSecretKey() {
		 String key=null;
		 try {
		       Ini ini;   ini = new Ini(new File(GeneralCodes.configFilePath()));
			  key=ini.get("header", "SECRET_KEY");
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		 return key;
	 }
	public static String getMpesaSTKPushUrl() {
		 String key=null;
		 try {
		       Ini ini;   ini = new Ini(new File(GeneralCodes.configFilePath()));
			  key=ini.get("header", "MpesaSTKPushUrl");
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		 return key;
	 }
	
	public static String getSTKPushTransactionStatusUrl() {
		 String key=null;
		 try {
		       Ini ini;   ini = new Ini(new File(GeneralCodes.configFilePath()));
			  key=ini.get("header", "STKPushTransactionStatusUrl");
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		 return key;
	 }
	
	public static String getAPP_KEY() {
		 String key=null;
		 try {
		       Ini ini;   ini = new Ini(new File(GeneralCodes.configFilePath()));
			  key=ini.get("header", "APP_KEY");
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		 return key;
	 }
	public static String getAPP_SECRET() {
		 String key=null;
		 try {
		       Ini ini;   ini = new Ini(new File(GeneralCodes.configFilePath()));
			  key=ini.get("header", "APP_SECRET");
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		 return key;
	 }
	public static String getMpesaCallBackURL() {
		 String key=null;
		 try {
		       Ini ini;   ini = new Ini(new File(GeneralCodes.configFilePath()));
			  key=ini.get("header", "MpesaCallBackURL");
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		 return key;
	 }
	public static String getMpesaSTKPushPAybill() {
		 String key="";
		 try {
		       Ini ini;   ini = new Ini(new File(GeneralCodes.configFilePath()));
			  key=ini.get("header", "MpesaSTKPushPaybill");
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		 return key;
	 }
	public static String getMpesa_Passkey() {
		 String key="";
		 try {
		       Ini ini;   ini = new Ini(new File(GeneralCodes.configFilePath()));
			  key=ini.get("header", "Mpesa_Passkey");
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		 return key;
	 }
	public static String getEmailUrl() {
		 String key=null;
		 try {
		       Ini ini;   ini = new Ini(new File(GeneralCodes.configFilePath()));
			  key=ini.get("header", "EmailUrl");
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		 return key;
	 }
	
	public static String getLogExceptionsURL() {
		 String key=null;
		 try {
		       Ini ini;   ini = new Ini(new File(GeneralCodes.configFilePath()));
			  key=ini.get("header", "ms_email_ldap");
		} catch (IOException e) {
			//System.out.println(e.getMessage());
		}
		 return key;
	 }
	public static String getUpdateServiceURL() {
		 String key=null;
		 try {
		       Ini ini;   ini = new Ini(new File(GeneralCodes.configFilePath()));
			  key=ini.get("header", "ebiller_updates");
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		 return key;
	 }
	
	
	
	public static String getUsbBaseUrl() {
		 String url=null;
		 try {
		       Ini ini;   ini = new Ini(new File(GeneralCodes.configFilePath()));
			  url=ini.get("header", "esb_account_fetch_url");
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		 return url;
	 }
	
	public static String getPaymentUrl() {
		 String url=null;
		 try {
		       Ini ini;   ini = new Ini(new File(GeneralCodes.configFilePath()));
			  url=ini.get("header", "payment_base_url");
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		 return url;
	 }
	
	public static String getLogoPath() {
		 String url=null;
		 try {
		       Ini ini;   ini = new Ini(new File(GeneralCodes.configFilePath()));
			  url=ini.get("header", "Logo_Path");
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		 return url;
	 }
	
	public static String getExcelPath() {
		 String url=null;
		 try {
		       Ini ini;   ini = new Ini(new File(GeneralCodes.configFilePath()));
			  url=ini.get("header", "Excel_Path");
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		 return url;
	 }
	public static String getExceptionURL() {
		 String url=null;
		 try {
		       Ini ini;   ini = new Ini(new File(GeneralCodes.configFilePath()));
			  url=ini.get("header", "exception_file_url");
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		 return url;
	 }
	
	public static String getSMSBaseUrl() {
		 String url=null;
		 try {
		       Ini ini;   ini = new Ini(new File(GeneralCodes.configFilePath()));
			  url=ini.get("header", "sms_url_link");
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		 return url;
	 }
	
	public static String getDefaultImageUrl() {
		 String url=null;
		 try {
		       Ini ini;   ini = new Ini(new File(GeneralCodes.configFilePath()));
			  url=ini.get("header", "DefaultLogo");
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		 return url;
	 }
	public static String getUsbNtken() {
		 String url=null;
		 try {
		       Ini ini;   ini = new Ini(new File(GeneralCodes.configFilePath()));
			  url=ini.get("header", "Esb_NTKEN");
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		 return url;
	 }
	
	public static String getExcelReportURL() {
		 String url=null;
		 try {
		       Ini ini;   ini = new Ini(new File(GeneralCodes.configFilePath()));
			  url=ini.get("header", "Excel_Report");
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		 return url;
	 }
	public static String Customer_Number_validation() {
		 String url=null;
		 try {
		       Ini ini;   ini = new Ini(new File(GeneralCodes.configFilePath()));
			  url=ini.get("header", "Customer_Number_validation");
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		 return url;
	 }
	
	public static String getExcelReportURL_Team() {
		 String url=null;
		 try {
		       Ini ini;   ini = new Ini(new File(GeneralCodes.configFilePath()));
			  url=ini.get("header", "Excel_Report_Team");
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		 return url;
	 }
	
	public static String getEbillerapiURL() {
		 String url=null;
		 try {
		       Ini ini;   ini = new Ini(new File(GeneralCodes.configFilePath()));
			  url=ini.get("header", "ebillerapi");
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		 return url;
	 }
	
	public static String getsystem_socketURL() {
		 String url=null;
		 try {
		       Ini ini;   ini = new Ini(new File(GeneralCodes.configFilePath()));
			  url=ini.get("header", "system_socket");
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		 return url;
	 }
	
	public static String kplcURL() {
		 String url=null;
		 try {
		       Ini ini;   ini = new Ini(new File(GeneralCodes.configFilePath()));
			  url=ini.get("header", "kplc");
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		 return url;
	 }
	
	public static String ebiller_updatesURL() {
		 String url=null;
		 try {
		       Ini ini;   ini = new Ini(new File(GeneralCodes.configFilePath()));
			  url=ini.get("header", "ebiller_updates");
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		 return url;
	 }
	
	public static String ms_email_ldapURL() {
		 String url=null;
		 try {
		       Ini ini;   ini = new Ini(new File(GeneralCodes.configFilePath()));
			  url=ini.get("header", "ms_email_ldap");
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		 return url;
	 }
}
