package com.MpesaApi.Callback;

import java.util.Hashtable;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.MpesaApi.Mpesa.MpesaHelper;
import com.MpesaApi.common.ExternalFile;
import com.MpesaApi.common.GeneralCodes;
import com.MpesaApi.common.TimeManager;
import com.stanbic.Responses.Response;


@SpringBootApplication
@RequestMapping("/api") 
public class CallBack {

	
	 @RequestMapping(value = "/v1/CallBack", method = RequestMethod.POST)
	 public ResponseEntity<?> getBillersTeam(@RequestBody String jsondata) {
		
		 try {
		System.out.println("Mpesa calling back now reached here___________________");
		System.out.println(jsondata);
        JSONObject MainObject = new JSONObject(jsondata);
        JSONObject body=MainObject.getJSONObject("Body");
		JSONObject stkCallback=body.getJSONObject("stkCallback");
		
		 String eslip_no="";
         String type="RESPONSE";
         String ResponseCode=stkCallback.getInt("ResultCode")+"";//
         String MerchantRequestID=stkCallback.getString("MerchantRequestID");//
//        
//         String CheckoutRequestID=stkCallback.getString("CheckoutRequestID");//
//         String ResponseDescription=stkCallback.getString("ResultDesc");//
//        
//         if(ResponseCode.equalsIgnoreCase("0")) {
//        	 String status="Success";
//        	 JSONObject CallbackMetadata=stkCallback.getJSONObject("CallbackMetadata");
//        	 JSONArray Item=CallbackMetadata.getJSONArray("Item");
//        	 JSONObject Itemobject = Item.getJSONObject(1);
//        	 JSONObject Itemobject2 = Item.getJSONObject(4);
//        	 JSONObject Itemobject3 = Item.getJSONObject(0);
//        	 String mpesa_ref=Itemobject.getString("Value");
//        	
//        	 String amount=Itemobject3.getDouble("Value")+"";
//        	 MpesaHelper.updatePayments(type, MerchantRequestID, CheckoutRequestID, ResponseDescription, mpesa_ref, "PAID");
//            MpesaHelper.logMpesaRequestResponse("NA","NA",eslip_no,type,MerchantRequestID,ResponseCode,"NA",CheckoutRequestID,ResponseDescription,mpesa_ref,status);
//            String eslip=MpesaHelper.getEslipNumber(MerchantRequestID, CheckoutRequestID);
//            String phoneNumber=MpesaHelper.getPhone(MerchantRequestID, CheckoutRequestID);
//            String ebiller_ref=MpesaHelper.getEbillerRef(MerchantRequestID, CheckoutRequestID);
//            
// 			
//           //call payment api
//           Hashtable<String,String>map=new Hashtable<String,String>();
//			 map.put("EslipNo",eslip);
//			 map.put("PaidAmount",amount);
//			 map.put("PaymentRefNo",mpesa_ref);
//			 map.put("PaymentDate",TimeManager.getEmailTime());
//			 System.out.println("Payload==>>"+map);
//			 String response=GeneralCodes.postRequest1(map, ExternalFile.getPaymentUrl(), "JSON");
//			
//          
//			  System.out.println("Phone>>>>>>>>>>>>>>>>>>>>"+phoneNumber);
//	            String message="Success, payment for E-slip No. "+eslip+" is successful,M-PESA REF:"+mpesa_ref+" Amount Ksh. "+amount+" .e-ebiller Ref:"+ebiller_ref;
//	            Thread t = new Thread() {
//	 				public void run() {
//	 					MpesaHelper.SendOTPSMS("0"+phoneNumber.substring(3), eslip_no, message);
//	 				}
//	 			};
//	 			t.start();
//         }else {
//        	 
//         String status="Failed";
//         String mpesa_ref="";
//         MpesaHelper.updatePayments(type, MerchantRequestID, CheckoutRequestID, ResponseDescription, mpesa_ref, "Cancelled");
//         MpesaHelper.logMpesaRequestResponse("NA","NA",eslip_no,type,MerchantRequestID,ResponseCode,"NA",CheckoutRequestID,ResponseDescription,mpesa_ref,status);
//         }
		
		return null;
		 }catch(Exception e) {
			 e.printStackTrace();
			 return ResponseEntity.ok(new Response("02","System technical error"));
		 }
}
}
