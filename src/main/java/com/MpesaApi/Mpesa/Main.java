package com.MpesaApi.Mpesa;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Hashtable;

import org.json.JSONObject;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.MpesaApi.common.DbManager;
import com.MpesaApi.common.ExternalFile;
import com.stanbic.Responses.Response;

@SpringBootApplication
@RequestMapping("/api")  
public class Main {
	 Connection conn=null;

     @RequestMapping(value = "/v1/STKPush", method = RequestMethod.POST)
	 public ResponseEntity<?> STKPush(@RequestBody MpesaApiConsume jsondata) {
    	 if(jsondata.getAmount().isEmpty() || jsondata.getPhone().isEmpty()) {
    		 return ResponseEntity.ok(new Response("06","Please provide phone number and amount"));
    	 }
    	 if(jsondata.getEslip_no().isEmpty()) {
    		 return ResponseEntity.ok(new Response("06","Provide eslip Number"));
    	 }
    	 try {
    		
    		 String paybill="";
    		 String callbackURL=ExternalFile.getMpesaCallBackURL();
    		 String amount=jsondata.getAmount();
    		 String phone="254"+jsondata.getPhone().substring(1);
    		 String password="";
              String comp_code=MpesaHelper.getComp_code(jsondata.getEslip_no());
              String app_key="";
              String app_secret="";
             
    		 
    		 conn = DbManager.getConnection();
 			String sql = "SELECT * FROM mpesa_credentials WHERE comp_code=?";
 			PreparedStatement prep = conn.prepareStatement(sql);
 			prep.setObject(1, comp_code);
 			ResultSet rs=prep.executeQuery();
 			while(rs.next()) { 
 				password=rs.getString("pass_key").trim();
 				paybill=rs.getString("paybill").trim();
 				app_key=rs.getString("app_key");
 				app_secret=rs.getString("app_secret");
 			}
 			conn.close();
    		System.out.println("Appkey>>>>>>>>>>>>"+app_key);
    		System.out.println("app_secret>>>>>>>>"+app_secret);
    		System.out.println("Password>>>>>>>>"+password);
    		System.out.println("paybill>>>>>>>>"+paybill);
//    		 Mpesa m=new Mpesa(Constants.APP_KEY,Constants.APP_SECRET);
    		Object response= Mpesa.STKPushSimulation(paybill,password,"20170824155055","CustomerPayBillOnline",amount,phone,phone,paybill,
    				callbackURL,callbackURL,jsondata.getEslip_no(),"8730",app_key.trim(),app_secret.trim());
    		
    		Hashtable<String,Object>map=new Hashtable<String,Object>();
    		 map.put("response",response);
    		 return ResponseEntity.ok(map);
    	 }catch(Exception e) {
    		e.printStackTrace();
       		 return ResponseEntity.ok(new Response("02","System technical error"));
    	 }
	
}
     
//     @RequestMapping(value = "/v1/STKPushEnquery", method = RequestMethod.POST)
//	 public ResponseEntity<?> STKPushEnquery(@RequestBody MpesaApiConsume jsondata) {
//    	 if(jsondata.getCheckoutrequestid().isEmpty()) {
//    		 return ResponseEntity.ok(new Response("06","Provide checkout request id"));
//    	 }
//    	 try {
//    		 String paybill=ExternalFile.getMpesaSTKPushPAybill();
//    		 String checkoutrequestid=jsondata.getCheckoutrequestid();
//    		 String password="MTc0Mzc5YmZiMjc5ZjlhYTliZGJjZjE1OGU5N2RkNzFhNDY3Y2QyZTBjODkzMDU5YjEwZjc4ZTZiNzJhZGExZWQyYzkxOTIwMTcwODI0MTU1MDU1";
//    		 Mpesa m=new Mpesa(Constants.APP_KEY,Constants.APP_SECRET);
//    		Object response=   m.STKPushTransactionStatus(paybill,password,"20170824155055",checkoutrequestid);
//    		
//    		Hashtable<String,Object>map=new Hashtable<String,Object>();
//    		 map.put("response",response);
//    		 return ResponseEntity.ok(map);
//    	 }catch(Exception e) {
//    		e.printStackTrace();
//       		 return ResponseEntity.ok(new Response("02","System technical error"));
//    	 }
//     }
     
//     @RequestMapping(value = "/v1/C2B", method = RequestMethod.POST)
//	 public ResponseEntity<?> C2B(@RequestBody MpesaApiConsume jsondata) {
//    	 if(jsondata.getAmount().isEmpty() || jsondata.getPhone().isEmpty()) {
//    		 return ResponseEntity.ok(new Response("06","Please provide phone and amount"));
//    	 }
//    	 try {
//    		 String paybill="600576";
//    		 String amount=jsondata.getAmount();
//    		 String phone="254"+jsondata.getPhone().substring(1);
//    		 Mpesa m=new Mpesa(Constants.APP_KEY,Constants.APP_SECRET);
////    		Object response= m.C2BSimulation(paybill,"CustomerPayBillOnline","2",phone,"hkjhjkhjkh",callbackURL);
//    		 Object response= m.registerURL(paybill,"Cancelled",callbackURL,callbackURL);
//
//    		Hashtable<String,Object>map=new Hashtable<String,Object>();
//    		 map.put("messageCode","00");
//    		 map.put("message","Querry success");
//    		 map.put("response",response);
//    		 return ResponseEntity.ok(map);
//    	 }catch(Exception e) {
//    		e.printStackTrace();
//       		 return ResponseEntity.ok(new Response("02","System technical error"));
//    	 }
//	
//}


        @RequestMapping(value = "/v1/b2c", method = RequestMethod.POST)
   	 public ResponseEntity<?> getBillersTeam(@RequestBody String jsondata) {
   		
   		 try {
   		
   			
//           m.authenticate();
          // 
           //m.authenticate();*/
   			 String password="A3oPT/9gAXrJPmex2YhmWmaAP75XmuTgVZicg5TWU/QQTiTGWCCVffUu2yJA4G8RB9mVRZXNzLclRnAZ6iEWuQ/4DuXq8oHP57wq9uMtjv3c8WDRjv+YNwFDa3RUox5v1Pj4MMqT/oLjBWP/XgoKqBNwXGODBz616HjJoC+FkQGB7wHFf2bX9t/1gpzyMMMGOJ7/Y4gebIEJPqBvpu82GBIU0yy/wqDWPhhf0+xrpfOUfFBRpzNIXc7tjv0PAdH8knCn+h+yY72BjSCHDqAWQZZjUI2VlvpHqwyQ9EsjcTwX4NGen+idIKw8sVkxCCeDIGQ2BuGiBYjnK0eXIYij1g==";
   
  			Mpesa.B2CRequest("jowuonda",password,"PromotionPayment","20","4069007","254712083128","This","https://b7b7d1da1d84.ngrok.io/api/auth/b2cResult","https://b7b7d1da1d84.ngrok.io/api/auth/b2cResult","https://b7b7d1da1d84.ngrok.io/api/auth/b2cResult");
   
   /*
           m.B2BRequest("testapi","his","BVeDP3XWGFG+NCQri04jHp6c0rCajO1JAOccQ7Bsu/Mup3Rh2Gd9IHQEE0SeA1oBXAt/VBAL/cJP+VKU9qRF6voqCa0P1XG8pcv5hTZUcBkbbb8Qqvqn28+s/tBvsLXwsB4QaageFDDZgS6b6gbK1p7+UZ/hRYHL8WclTpYBrQGfhqKZxduh0bPWvK4rt+uqR3hdVlO0RdJSkcOVCVp+FxizPSk3nI6LFq14Jj2G0TwuQ4a13J/KVu5eeFG65gzE1NnIVouHKeBPz9b9xvove156aR16uxh4rBq5U6UAKC/kUhaJ0wOLTvb762CioudL87C6xaPVdTF4qcSD6jM4PA==","BusinessPayBill","1", "4",22,"600576","600000","This","http://obscure-bayou-52273.herokuapp.com/api/Mpesa/C2BConfirmation","http://obscure-bayou-52273.herokuapp.com/api/Mpesa/C2BValidation","http://obscure-bayou-52273.herokuapp.com/api/Mpesa/C2BValidation");
   */

        
   /*
           m.reversal("testapi","BVeDP3XWGFG+NCQri04jHp6c0rCajO1JAOccQ7Bsu/Mup3Rh2Gd9IHQEE0SeA1oBXAt/VBAL/cJP+VKU9qRF6voqCa0P1XG8pcv5hTZUcBkbbb8Qqvqn28+s/tBvsLXwsB4QaageFDDZgS6b6gbK1p7+UZ/hRYHL8WclTpYBrQGfhqKZxduh0bPWvK4rt+uqR3hdVlO0RdJSkcOVCVp+FxizPSk3nI6LFq14Jj2G0TwuQ4a13J/KVu5eeFG65gzE1NnIVouHKeBPz9b9xvove156aR16uxh4rBq5U6UAKC/kUhaJ0wOLTvb762CioudL87C6xaPVdTF4qcSD6jM4PA==","TransactionReversal","2121","2","22","4","http://obscure-bayou-52273.herokuapp.com/api/Mpesa/C2BValidation","http://obscure-bayou-52273.herokuapp.com/api/Mpesa/C2BConfirmation","Remarks","Ocassions");
   */
   /* 
           m.registerURL("600576","Cancelled","http://obscure-bayou-52273.herokuapp.com/api/Mpesa/C2BValidation","http://obscure-bayou-52273.herokuapp.com/api/Mpesa/C2BValidation");
   */
   /*
           System.out.println("Hello World!");
   */
   /*
           m.balanceInquiry("testapi","AccountBalance","BVeDP3XWGFG+NCQri04jHp6c0rCajO1JAOccQ7Bsu/Mup3Rh2Gd9IHQEE0SeA1oBXAt/VBAL/cJP+VKU9qRF6voqCa0P1XG8pcv5hTZUcBkbbb8Qqvqn28+s/tBvsLXwsB4QaageFDDZgS6b6gbK1p7+UZ/hRYHL8WclTpYBrQGfhqKZxduh0bPWvK4rt+uqR3hdVlO0RdJSkcOVCVp+FxizPSk3nI6LFq14Jj2G0TwuQ4a13J/KVu5eeFG65gzE1NnIVouHKeBPz9b9xvove156aR16uxh4rBq5U6UAKC/kUhaJ0wOLTvb762CioudL87C6xaPVdTF4qcSD6jM4PA==", "600576","4","These","http://obscure-bayou-52273.herokuapp.com/api/Mpesa/C2BConfirmation","http://obscure-bayou-52273.herokuapp.com/api/Mpesa/C2BConfirmation");
   */
   /*
           m.STKPushTransactionStatus("174379","MTc0Mzc5YmZiMjc5ZjlhYTliZGJjZjE1OGU5N2RkNzFhNDY3Y2QyZTBjODkzMDU5YjEwZjc4ZTZiNzJhZGExZWQyYzkxOTIwMTcwODI0MTU1MDU1","20170824155055","ws_CO_27102017101215530");
   */
   			 
   			 
         return ResponseEntity.ok(new Response("00","Success"));
   		 }catch(Exception e) {
   		
   		 }
		return null;
   }
        
}
