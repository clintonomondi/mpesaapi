package com.MpesaApi.Mpesa;

import okhttp3.*;
import org.json.*;

import com.MpesaApi.common.ExternalFile;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.Base64;
import java.util.Hashtable;

public class Mpesa {
//    private String appKey;
//    private String appSecret;
//    public Mpesa(String app_key, String app_secret){
//        appKey=app_key;
//        appSecret=app_secret;
//    }
//
//    public String getAppKey() {
//        return appKey;
//    }
//
//    public void setAppKey(String appKey) {
//        this.appKey = appKey;
//    }
//
//    public String getAppSecret() {
//        return appSecret;
//    }
//
//    public void setAppSecret(String appSecret) {
//        this.appSecret = appSecret;
//    }

    public static String authenticate(String appKey,String appSecret) throws IOException {
    	System.out.println("Appkey22>>>>>>>>>>>>"+appKey);
		System.out.println("app_secret22>>>>>>>>"+appSecret);
  
        String appKeySecret = appKey + ":" + appSecret;
        byte[] bytes = appKeySecret.getBytes("ISO-8859-1");
        String encoded = Base64.getEncoder().encodeToString(bytes);

        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url("https://api.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials")
                .get()
                .addHeader("authorization", "Basic "+encoded)
                .addHeader("cache-control", "no-cache")
                .build();
        Response response = client.newCall(request).execute();
        System.out.println("AuthResponse=====>"+response);
        JSONObject jsonObject=new JSONObject(response.body().string());
        System.out.println("Access Token---->"+jsonObject.getString("access_token"));
        return jsonObject.getString("access_token");
    }
    public static Object STKPushSimulation(String businessShortCode, String password, String timestamp,String transactionType, String amount, 
    		String phoneNumber, String partyA, String partyB, String callBackURL, String queueTimeOutURL,String accountReference, String 
    		transactionDesc,String key,String secret) throws IOException {
        String password_to_send=Base64.getEncoder().encodeToString((businessShortCode+password+timestamp).getBytes());
      	JSONArray jsonArray=new JSONArray();
          JSONObject jsonObject=new JSONObject();
          jsonObject.put("BusinessShortCode", businessShortCode);
          jsonObject.put("Password", password_to_send);
          jsonObject.put("Timestamp", timestamp);
          jsonObject.put("TransactionType", transactionType);
          jsonObject.put("Amount",amount);
          jsonObject.put("PhoneNumber", phoneNumber);
          jsonObject.put("PartyA", partyA);
          jsonObject.put("PartyB", partyB);
          jsonObject.put("CallBackURL", callBackURL);
          jsonObject.put("AccountReference", accountReference);
          jsonObject.put("QueueTimeOutURL", queueTimeOutURL);
          jsonObject.put("TransactionDesc", transactionDesc);

          
          jsonArray.put(jsonObject);
          String requestJson=jsonArray.toString().replaceAll("[\\[\\]]","");

          OkHttpClient client = new OkHttpClient();
          String url=ExternalFile.getMpesaSTKPushUrl();
          MediaType mediaType = MediaType.parse("application/json");
          RequestBody body = RequestBody.create(mediaType, requestJson);
          Request request = new Request.Builder()
                  .url(url)
                  .post(body)
                  .addHeader("content-type", "application/json")
                  .addHeader("authorization", "Bearer "+authenticate(key,secret))
                  .addHeader("cache-control", "no-cache")
                  .build();
          Response response = client.newCall(request).execute();
          
          String jsonData = response.body().string();
          JSONObject Jobject = new JSONObject(jsonData);
         
            
         System.out.println("Response"+Jobject);
         Hashtable<String,Object>map=new Hashtable<String,Object>();
         
         if(Jobject.has("ResponseCode")) {
             String eslip_no=accountReference;
             String type="REQUEST";
             String ResponseCode=Jobject.getString("ResponseCode");
             String MerchantRequestID=Jobject.getString("MerchantRequestID");
             String CustomerMessage=Jobject.getString("CustomerMessage");
             String CheckoutRequestID=Jobject.getString("CheckoutRequestID");
             String ResponseDescription=Jobject.getString("ResponseDescription");
             String mpesa_ref="";
             String status="Success";
             String ebiller_ref=MpesaHelper.getRandomText();
             
             MpesaHelper.logPayments(phoneNumber,amount,eslip_no,type,MerchantRequestID,ResponseCode,CustomerMessage,CheckoutRequestID,ResponseDescription,mpesa_ref,"Pending",ebiller_ref);
             MpesaHelper.logMpesaRequestResponse(phoneNumber,amount,eslip_no,type,MerchantRequestID,ResponseCode,CustomerMessage,CheckoutRequestID,ResponseDescription,mpesa_ref,status);
             //send sms to user
             String message="M-pesa Payment for Eslip No. "+eslip_no+" Amount Ksh. "+amount+".00  has been initiated,e-biller ref: "+ebiller_ref+" enter your M-pesa pin to complete transactions.";
             Thread t = new Thread() {
  				public void run() {
  					MpesaHelper.SendOTPSMS("0"+phoneNumber.substring(3), eslip_no, message);
  				}
  			};
  			t.start();
      	   
    		 map.put("MerchantRequestID",Jobject.getString("MerchantRequestID"));
    		 map.put("CheckoutRequestID",Jobject.getString("CheckoutRequestID"));
    		 map.put("ResponseCode",Jobject.getString("ResponseCode"));
    		 map.put("ResponseDescription",Jobject.getString("ResponseDescription"));
    		 map.put("CustomerMessage",Jobject.getString("CustomerMessage"));
    		map.put("message",Jobject.getString("CustomerMessage"));
      	map.put("messageCode","00");
         }
         if(Jobject.has("errorCode")) {
      	   
             String eslip_no=accountReference;
             String type="REQUEST";
             String ResponseCode=Jobject.getString("errorCode");//
             String MerchantRequestID=Jobject.getString("requestId");//
             String CustomerMessage=Jobject.getString("errorMessage");//
             String mpesa_ref="";
             String status="Failed";
//             MpesaHelper.logMpesaRequestResponse(phoneNumber,amount,eslip_no,type,MerchantRequestID,ResponseCode,CustomerMessage,"NA","NA",mpesa_ref,status);
             
      	 map.put("requestId",Jobject.getString("requestId"));
      	map.put("errorMessage",Jobject.getString("errorMessage"));
      	map.put("errorCode",Jobject.getString("errorCode"));
      	map.put("message",Jobject.getString("errorMessage"));
      	map.put("messageCode","06");
         }
          return map;
      }
//    public Object C2BSimulation( String shortCode, String commandID, String amount, String MSISDN, String billRefNumber,String callBackURL) throws IOException {
//        JSONArray jsonArray=new JSONArray();
//        JSONObject jsonObject=new JSONObject();
//        jsonObject.put("ShortCode", shortCode);
//        jsonObject.put("CommandID", commandID);
//        jsonObject.put("Amount", amount);
//        jsonObject.put("Msisdn", MSISDN);
//        jsonObject.put("BillRefNumber", billRefNumber);
//
//        jsonArray.put(jsonObject);
//
//        String requestJson=jsonArray.toString().replaceAll("[\\[\\]]","");
//        OkHttpClient client = new OkHttpClient();
//
//        MediaType mediaType = MediaType.parse("application/json");
//        RequestBody body = RequestBody.create(mediaType, requestJson);
//        Request request = new Request.Builder()
//                .url("https://sandbox.safaricom.co.ke/mpesa/c2b/v1/simulate")
//                .post(body)
//                .addHeader("content-type", "application/json")
//                .addHeader("authorization", "Bearer "+authenticate())
//                .addHeader("cache-control", "no-cache")
//                .build();
//
//        Response response = client.newCall(request).execute();
//        String jsonData = response.body().string();
//        JSONObject Jobject = new JSONObject(jsonData);
//       System.out.println(Jobject);
//       
//       Hashtable<String,Object>map=new Hashtable<String,Object>();
//       if(Jobject.has("ResponseCode")) {
//  		 map.put("MerchantRequestID",Jobject.getString("MerchantRequestID"));
//  		 map.put("CheckoutRequestID",Jobject.getString("CheckoutRequestID"));
//  		 map.put("ResponseCode",Jobject.getString("ResponseCode"));
//  		 map.put("ResponseDescription",Jobject.getString("ResponseDescription"));
//  		 map.put("CustomerMessage",Jobject.getString("CustomerMessage"));
//       }
//       if(Jobject.has("errorCode")) {
//    	 map.put("requestId",Jobject.getString("requestId"));
//    	map.put("errorMessage",Jobject.getString("errorMessage"));
//    	map.put("errorCode",Jobject.getString("errorCode"));
//       }
//        return map;
//    }
//
    public static String B2CRequest( String initiatorName, String securityCredential,String commandID, String  amount, String partyA,String partyB, String remarks, String queueTimeOutURL, String resultURL, String occassion) throws IOException {
//    	String password_to_send=Base64.getEncoder().encodeToString((partyA+securityCredential+"20170824155055").getBytes());
    	JSONArray jsonArray=new JSONArray();
        JSONObject jsonObject=new JSONObject();
        jsonObject.put("InitiatorName", initiatorName);
        jsonObject.put("SecurityCredential", securityCredential);
        jsonObject.put("CommandID", commandID);
        jsonObject.put("Amount", amount);
        jsonObject.put("PartyA", partyA);
        jsonObject.put("PartyB", partyB);
        jsonObject.put("Remarks", remarks);
        jsonObject.put("QueueTimeOutURL", queueTimeOutURL);
        jsonObject.put("ResultURL", resultURL);
        jsonObject.put("Occassion", occassion);


        jsonArray.put(jsonObject);

        String requestJson=jsonArray.toString().replaceAll("[\\[\\]]","");


        OkHttpClient client = new OkHttpClient();
        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(mediaType, requestJson);
        Request request = new Request.Builder()
                .url( "https://api.safaricom.co.ke/mpesa/b2c/v1/paymentrequest")
                .post(body)
                .addHeader("content-type", "application/json")
                .addHeader("authorization", "Bearer "+authenticate("zduiAtRoqTnTQ0PGC2uGHxAlLmyPE0z9","7Aw9KYD38D1ctuAd"))
                .addHeader("cache-control", "no-cache")
                .build();

        Response response = client.newCall(request).execute();
        System.out.println(response.body().string());
        return response.body().toString();
    }

//    public String B2BRequest( String initiatorName, String accountReference,String securityCredential,String commandID, String senderIdentifierType,String receiverIdentifierType,float  amount, String partyA,String partyB, String remarks, String queueTimeOutURL, String resultURL, String occassion) throws IOException {
//        JSONArray jsonArray=new JSONArray();
//        JSONObject jsonObject=new JSONObject();
//        jsonObject.put("Initiator", initiatorName);
//        jsonObject.put("SecurityCredential", securityCredential);
//        jsonObject.put("CommandID", commandID);
//        jsonObject.put("SenderIdentifierType", senderIdentifierType);
//        jsonObject.put("RecieverIdentifierType",receiverIdentifierType);
//        jsonObject.put("Amount", amount);
//        jsonObject.put("PartyA", partyA);
//        jsonObject.put("PartyB", partyB);
//        jsonObject.put("Remarks", remarks);
//        jsonObject.put("AccountReference", accountReference);
//        jsonObject.put("QueueTimeOutURL", queueTimeOutURL);
//        jsonObject.put("ResultURL", resultURL);
//
//
//
//        jsonArray.put(jsonObject);
//
//        String requestJson=jsonArray.toString().replaceAll("[\\[\\]]","");
//        System.out.println(requestJson);
//
//        OkHttpClient client = new OkHttpClient();
//
//        MediaType mediaType = MediaType.parse("application/json");
//        RequestBody body = RequestBody.create(mediaType, requestJson);
//        Request request = new Request.Builder()
//                .url("https://sandbox.safaricom.co.ke/safaricom/b2b/v1/paymentrequest")
//                .post(body)
//                .addHeader("content-type", "application/json")
//                .addHeader("authorization", "Bearer "+authenticate())
//                .addHeader("cache-control", "no-cache")
//
//                .build();
//
//        Response response = client.newCall(request).execute();
//        return response.body().string();
//
//    }
  
    
     
//    public Object STKPushTransactionStatus( String businessShortCode, String password, String timestamp, String checkoutRequestID) throws IOException {
//        JSONArray jsonArray=new JSONArray();
//        JSONObject jsonObject=new JSONObject();
//        jsonObject.put("BusinessShortCode", businessShortCode);
//        jsonObject.put("Password", password);
//        jsonObject.put("Timestamp", timestamp);
//        jsonObject.put("CheckoutRequestID", checkoutRequestID);
//
//
//        jsonArray.put(jsonObject);
//
//        String requestJson=jsonArray.toString().replaceAll("[\\[\\]]","");
//
//
//        OkHttpClient client = new OkHttpClient();
//
//        MediaType mediaType = MediaType.parse("application/json");
//        RequestBody body = RequestBody.create(mediaType, requestJson);
//        Request request = new Request.Builder()
//                .url(ExternalFile.getSTKPushTransactionStatusUrl())
//                .post(body)
//                .addHeader("authorization", "Bearer "+authenticate())
//                .addHeader("content-type", "application/json")
//                .build();
//
//        Response response = client.newCall(request).execute();
//        String jsonData = response.body().string();
//        JSONObject Jobject = new JSONObject(jsonData);
//        System.out.println(Jobject);
//        Hashtable<String,Object>map=new Hashtable<String,Object>();
//        
//        if(Jobject.has("ResponseCode")) {
//          
//   		 map.put("CheckoutRequestID",Jobject.getString("CheckoutRequestID"));
//   		map.put("message",Jobject.getString("ResultDesc"));
//     	map.put("messageCode","00");
//        }
//        if(Jobject.has("errorCode")) {
//     	  
//     	map.put("errorCode",Jobject.getString("errorCode"));
//     	map.put("message",Jobject.getString("errorMessage"));
//     	map.put("messageCode","06");
//        }
//         return map;
//
//    }
//    public String reversal(String initiator, String securityCredential, String commandID, String transactionID, String amount, String receiverParty, String recieverIdentifierType, String resultURL,String queueTimeOutURL, String remarks, String ocassion) throws IOException {
//        JSONArray jsonArray=new JSONArray();
//        JSONObject jsonObject=new JSONObject();
//        jsonObject.put("Initiator", initiator);
//        jsonObject.put("SecurityCredential", securityCredential);
//        jsonObject.put("CommandID", commandID);
//        jsonObject.put("TransactionID", transactionID);
//        jsonObject.put("Amount",amount);
//        jsonObject.put("ReceiverParty", receiverParty);
//        jsonObject.put("RecieverIdentifierType", recieverIdentifierType);
//        jsonObject.put("QueueTimeOutURL", queueTimeOutURL);
//        jsonObject.put("ResultURL", resultURL);
//        jsonObject.put("Remarks", remarks);
//        jsonObject.put("Occasion", ocassion);
//
//
//
//        jsonArray.put(jsonObject);
//
//        String requestJson=jsonArray.toString().replaceAll("[\\[\\]]","");
//        System.out.println(requestJson);
//
//        OkHttpClient client = new OkHttpClient();
//
//        MediaType mediaType = MediaType.parse("application/json");
//        RequestBody body = RequestBody.create(mediaType, requestJson);
//        Request request = new Request.Builder()
//                .url("https://sandbox.safaricom.co.ke/safaricom/reversal/v1/request")
//                .post(body)
//                .addHeader("content-type", "application/json")
//                .addHeader("authorization", "Bearer xNA3e9KhKQ8qkdTxJJo7IDGkpFNV")
//                .addHeader("cache-control", "no-cache")
//                .build();
//
//        Response response = client.newCall(request).execute();
//        System.out.println(response.body().string());
//        return response.body().string();
//    }
//    public String balanceInquiry(String initiator, String commandID, String securityCredential, String partyA, String identifierType, String remarks, String queueTimeOutURL, String resultURL) throws IOException {
//        JSONArray jsonArray=new JSONArray();
//        JSONObject jsonObject=new JSONObject();
//        jsonObject.put("Initiator", initiator);
//        jsonObject.put("SecurityCredential", securityCredential);
//        jsonObject.put("CommandID", commandID);
//        jsonObject.put("PartyA", partyA);
//        jsonObject.put("IdentifierType",identifierType);
//        jsonObject.put("Remarks", remarks);
//        jsonObject.put("QueueTimeOutURL", queueTimeOutURL);
//        jsonObject.put("ResultURL", resultURL);
//
//
//
//
//        jsonArray.put(jsonObject);
//
//        String requestJson=jsonArray.toString().replaceAll("[\\[\\]]","");
//        System.out.println(requestJson);
//
//        OkHttpClient client = new OkHttpClient();
//
//        MediaType mediaType = MediaType.parse("application/json");
//        RequestBody body = RequestBody.create(mediaType, requestJson);
//        Request request = new Request.Builder()
//                .url("https://sandbox.safaricom.co.ke/safaricom/accountbalance/v1/query")
//                .post(body)
//                .addHeader("content-type", "application/json")
//                .addHeader("authorization", "Bearer fwu89P2Jf6MB1A2VJoouPg0BFHFM")
//                .addHeader("cache-control", "no-cache")
//                .addHeader("postman-token", "2aa448be-7d56-a796-065f-b378ede8b136")
//                .build();
//
//        Response response = client.newCall(request).execute();
//        return response.body().string();
//    }
//    public  String registerURL(String shortCode, String responseType, String confirmationURL, String validationURL) throws IOException {
//        JSONArray jsonArray=new JSONArray();
//        JSONObject jsonObject=new JSONObject();
//        jsonObject.put("ShortCode", shortCode);
//        jsonObject.put("ResponseType", responseType);
//        jsonObject.put("ConfirmationURL", confirmationURL);
//        jsonObject.put("ValidationURL", validationURL);
//
//        jsonArray.put(jsonObject);
//
//        String requestJson=jsonArray.toString().replaceAll("[\\[\\]]","");
//       
//        OkHttpClient client = new OkHttpClient();
//
//        MediaType mediaType = MediaType.parse("application/json");
//        RequestBody body = RequestBody.create(mediaType, requestJson);
//        Request request = new Request.Builder()
//                .url("https://sandbox.safaricom.co.ke/mpesa/c2b/v1/registerurl")
//                .post(body)
//                .addHeader("content-type", "application/json")
//                .addHeader("authorization", "Bearer "+authenticate())
//                .addHeader("cache-control", "no-cache")
//                .build();
//
//        Response response = client.newCall(request).execute();
//        System.out.println(response.body().string());
//        return response.body().string();
//    }

}
