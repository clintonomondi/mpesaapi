package com.MpesaApi.status;

import java.util.Hashtable;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@SpringBootApplication
@RequestMapping("/api")
public class status {

	 @RequestMapping(value = "/v1/checkStatus", method = RequestMethod.POST)
	 public ResponseEntity<?> checkStatus(@RequestBody String jsondata){
		 
		 Hashtable<Object, Object> map = new Hashtable<Object, Object>();
			map.put("responseCode", "00");
			map.put("responseMessage", "success");
			return ResponseEntity.ok(map);
	 }
}
